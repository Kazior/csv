
require 'csv'

class Datasheet

	def process
		load_data
		calculate
		save_data
	end

	def load_data(filename = "./data.csv")
		@temperature = CSV.read(filename, headers:true, converters: :numeric)
		@cities = @temperature.headers
	end

	def calculate
		@temperature.each_with_index do |row, index|
			x = 0
			@cities.each do |city|
				x += row[city]
			end

			@average = x / @cities.size
			@temperature[index] << { average: @average }
		end
	end

	def save_data(filename = "./output.csv")		
		update_headers
		
		CSV.open(filename, 'w') do |csv_object|
  		csv_object << @headers
  	 	
  	 	@temperature.each do |row|
   			csv_object << row
  		end
		end
	end

	def update_headers
		@headers = @cities
		@headers << "average"
	end
end
